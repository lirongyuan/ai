"""Program to solve 8-puzzle board when given a starting configuration.

Usage:
$ python driver.py <method> <board>

The <method> should be one of the following:
- bfs (Breadth-First Search)
- dfs (Depth-First Search)
- ast (A-Star Search)

The <board> should be a comma-separated list of integers containing no spaces.
For example, 0,8,7,6,5,4,3,2,1

To use the breadth-first search strategy to solve the input board given by this
starting configuration, the program should be executed like so:
$ python driver.py bfs 0,8,7,6,5,4,3,2,1
"""

import sys

import heapq
import resource


class Node(object):
  """Node of the search tree being constructed."""

  __slots__ = ("board", "depth", "action", "parent")

  def __init__(self, board, action, depth=0, parent=None):
    self.board = board
    self.depth = depth
    self.action = action
    self.parent = parent

  def __eq__(self, other):
    """Override the default Equals behavior."""
    if isinstance(other, self.__class__):
      return self.board == other.board
    return NotImplemented

  def __ne__(self, other):
    """Define a non-equality test."""
    if isinstance(other, self.__class__):
      return not self.__eq__(other)
    return NotImplemented

  def __hash__(self):
    """Override the default hash behavior."""
    return hash(tuple(self.board))

  def __repr__(self):
    return "board: " + self.board + "depth: " + self.depth

  def is_valid(self):
    return len(self.board) == 9

  def is_goal(self):
    return self.board == [0, 1, 2, 3, 4, 5, 6, 7, 8]

  def child_node(self, state, result_state, action):
    board = self.board[:]
    temp = board[state]
    board[state] = board[result_state]
    board[result_state] = temp
    return Node(board, action, self.depth + 1, self)

  def path_to_self(self):
    node = self
    parent = node.parent
    paths = []
    while parent is not None:
      paths.insert(0, Action.DESCRIPTION[node.action])
      node = parent
      parent = node.parent
    return paths

class Action(object):
  """Possible Movements of the blank space: Left, Right, Up, or Down."""

  UP = 0
  DOWN = 1
  LEFT = 2
  RIGHT = 3
  DESCRIPTION = {
      UP: "UP",
      DOWN: "DOWN",
      LEFT: "LEFT",
      RIGHT: "RIGHT"
  }


class State(object):
  """The location of the blank in one of the nine squares."""

  INVALID = -1

  @staticmethod
  def state_after_action(state, action):
    """Transition mode: give a state and action, this returns the resulting
       state."""

    x = state % 3
    y = state / 3
    result_state = State.INVALID
    if action == 0 and y > 0:
      result_state = state - 3
    elif action == 1 and y < 2:
      result_state = state + 3
    elif action == 2 and x > 0:
      result_state = state - 1
    elif action == 3 and x < 2:
      result_state = state + 1
    return result_state


class GraphSearch(object):
  """Graph search algorithm that expands nodes on the frontier until a solution
     is found or there are no more states to expand."""

  __slots__ = ("root", "path_to_goal", "cost_of_path", "nodes_expanded",
               "search_depth", "max_search_depth")

  def __init__(self, root, cost_of_path=0, nodes_expanded=0,
               search_depth=0, max_search_depth=0):
    self.root = root
    self.path_to_goal = []
    self.cost_of_path = cost_of_path
    self.nodes_expanded = nodes_expanded
    self.search_depth = search_depth
    self.max_search_depth = max_search_depth
    self.search()

  def __repr__(self):
    running_time = (resource.getrusage(resource.RUSAGE_SELF).ru_utime +
                    resource.getrusage(resource.RUSAGE_SELF).ru_stime)
    max_ram_usage = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
    return ("path_to_goal: %s\ncost_of_path: %d\nnodes_expanded: %d\n"
            "search_depth: %d\nmax_search_depth: %d\nrunning_time: %f\n"
            "max_ram_usage: %f") % (
                self.path_to_goal, self.cost_of_path, self.nodes_expanded,
                self.search_depth, self.max_search_depth, running_time,
                max_ram_usage)


class BFS(GraphSearch):
  """An instance of graph search in which the shallowest unexpanded node is
     chosen for expansion."""

  def search(self):
    frontier = [self.root]
    visited = {self.root: True}
    while frontier:
      node = frontier.pop(0)
      if node.is_goal():
        self.path_to_goal = node.path_to_self()
        self.cost_of_path = node.depth
        self.search_depth = node.depth
        self.nodes_expanded = len(visited) - len(frontier) - 1
        return
      state = node.board.index(0)
      actions = [Action.UP, Action.DOWN, Action.LEFT, Action.RIGHT]
      for action in actions:
        result_state = State.state_after_action(state, action)
        if result_state != State.INVALID:
          child = node.child_node(state, result_state, action)
          if child not in visited:
            self.max_search_depth = max(child.depth, self.max_search_depth)
            frontier.append(child)
            visited[child] = True


class DFS(GraphSearch):
  """An instance of graph search in which the deepest node in the current
     frontier is chosen for expansion."""

  def search(self):
    frontier = [self.root]
    visited = {self.root: True}
    while frontier:
      node = frontier.pop()
      if node.is_goal():
        self.path_to_goal = node.path_to_self()
        self.cost_of_path = node.depth
        self.search_depth = node.depth
        self.nodes_expanded = len(visited) - len(frontier) - 1
        return
      state = node.board.index(0)
      actions = [Action.RIGHT, Action.LEFT, Action.DOWN, Action.UP]
      for action in actions:
        result_state = State.state_after_action(state, action)
        if result_state != State.INVALID:
          child = node.child_node(state, result_state, action)
          if child not in visited:
            self.max_search_depth = max(child.depth, self.max_search_depth)
            frontier.append(child)
            visited[child] = True


class AST(GraphSearch):
  """An instance of graph search in which the node with the least estimated
     cost is chosen for expansion."""

  def manhattan_priority(self, node):
    priority = 0
    i = 1
    while i < 9:
      index = node.board.index(i)
      priority += abs((index/3) - i/3)
      priority += abs((index%3) - i%3)
      i += 1
    return priority

  def estimated_cost(self, node):
    return node.depth + self.manhattan_priority(node)

  def search(self):
    frontier = []
    heapq.heappush(frontier, (self.estimated_cost(self.root), self.root))
    visited = {self.root: True}
    while frontier:
      node_tuple = heapq.heappop(frontier)
      node = node_tuple[1]
      if node.is_goal():
        self.path_to_goal = node.path_to_self()
        self.cost_of_path = node.depth
        self.search_depth = node.depth
        self.nodes_expanded = len(visited) - len(frontier) - 1
        return
      state = node.board.index(0)
      actions = [Action.UP, Action.DOWN, Action.LEFT, Action.RIGHT]
      for action in actions:
        result_state = State.state_after_action(state, action)
        if result_state != State.INVALID:
          child = node.child_node(state, result_state, action)
          if child not in visited:
            self.max_search_depth = max(child.depth, self.max_search_depth)
            heapq.heappush(frontier, (self.estimated_cost(child), child))
            visited[child] = True

if __name__ == "__main__":
  if len(sys.argv) == 3:
    method = sys.argv[1]
    root_node = Node(map(int, sys.argv[2].split(",")), 0)
    if root_node.is_valid():
      f = open("output.txt", "w")
      if method == "bfs":
        f.write(str(BFS(root_node)))
      elif method == "dfs":
        f.write(str(DFS(root_node)))
      elif method == "ast":
        f.write(str(AST(root_node)))
      f.close()
