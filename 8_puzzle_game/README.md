## 8-puzzle Game

An instance of the N-puzzle game consists of a board holding N = m^2 − 1 (m = 3, 4, 5, ...) distinct movable tiles, plus an empty space. The tiles are numbers from the set {1, …, m^2 − 1}. For any such board, the empty space may be legally swapped with any tile horizontally or vertically adjacent to it. In this program, we will represent the blank space with the number 0 and focus on the m = 3 case (8-puzzle).

Given an initial state of the board, the combinatorial search problem is to find a sequence of moves that transitions this state to the goal state; that is, the configuration with all tiles arranged in ascending order ⟨0, 1, …, m^2 − 1⟩. The search space is the set of all possible states reachable from the initial state.

The blank space may be swapped with a component in one of the four directions {‘Up’, ‘Down’, ‘Left’, ‘Right’}, one move at a time. The cost of moving from one configuration of the board to another is the same and equal to one. Thus, the total cost of path is equal to the number of moves made from the initial state to the goal state.

`driver.py` solves any 8-puzzle board when given an arbitrary starting configuration. The program will be executed as follows:

```
$ python driver.py <method> <board>
```

The method argument will be one of the following:

- bfs (Breadth-First Search)
- dfs (Depth-First Search)
- ast (A-Star Search)

The board argument will be a comma-separated list of integers containing no spaces. For example, to use the bread-first search strategy to solve the input board given by the starting configuration `{0,8,7,6,5,4,3,2,1}`, the program will be executed like so (with no spaces between commas):

```
$ python driver.py bfs 0,8,7,6,5,4,3,2,1
```

When executed, the program will create and write to a file called output.txt, containing the following statistics:

- path_to_goal: the sequence of moves taken to reach the goal
- cost_of_path: the number of moves taken to reach the goal
- nodes_expanded: the number of nodes that have been expanded
- search_depth: the depth within the search tree when the goal node is found
- max_search_depth:  the maximum depth of the search tree in the lifetime of the algorithm
- running_time: the total running time of the search instance, reported in seconds
- max_ram_usage: the maximum RAM usage in the lifetime of the process as measured by the ru_maxrss attribute in the resource module, reported in megabytes

### Example 1: Breadth-First Search
Suppose the program is executed for breadth-first search as follows:

```
$ python driver.py bfs 1,2,5,3,4,0,6,7,8
```

The output file (example) will contain exactly the following lines:

```
path_to_goal: ['Up', 'Left', 'Left']
cost_of_path: 3
nodes_expanded: 10
search_depth: 3
max_search_depth: 4
running_time: 0.00188088
max_ram_usage: 0.07812500
```

The following two test cases are used for program validation. Note that long path_to_goals are truncated and running_time/max_ram_usage are removed:

```
python driver.py bfs 6,1,8,4,0,2,7,3,5

path_to_goal: ['Down', 'Right', 'Up', 'Up', 'Left', 'Down', 'Right', 'Down', 'Left', 'Up', 'Left', 'Up', 'Right', 'Right', 'Down', 'Down', 'Left', 'Left', 'Up', 'Up']
cost_of_path: 20
nodes_expanded: 54094
search_depth: 20
max_search_depth: 21
```

```
python driver.py bfs 8,6,4,2,1,3,5,7,0

path_to_goal: ['Left', 'Up', 'Up', 'Left', 'Down', 'Right', 'Down', 'Left', 'Up', 'Right', 'Right', 'Up', 'Left', 'Left', 'Down', 'Right', 'Right', 'Up', 'Left', 'Down', 'Down', 'Right', 'Up', 'Left', 'Up', 'Left']
cost_of_path: 26
nodes_expanded: 166786
search_depth: 26
max_search_depth: 27
```

### Example 2: Depth-First Search

Suppose the program is executed for depth-first search as follows:

```
$ python driver.py dfs 1,2,5,3,4,0,6,7,8
```

The output file (example) will contain exactly the following lines:

```
path_to_goal: ['Up', 'Left', 'Left']
cost_of_path: 3
nodes_expanded: 181437
search_depth: 3
max_search_depth: 66125
running_time: 5.01608433
max_ram_usage: 4.23940217
```

The following two test cases are used for program validation. Note that long path_to_goals are truncated and running_time/max_ram_usage are removed:

```
python driver.py dfs 6,1,8,4,0,2,7,3,5

path_to_goal: ['Up', 'Left', 'Down', ... , 'Up', 'Left', 'Up', 'Left']
cost_of_path: 46142
nodes_expanded: 51015
search_depth: 46142
max_search_depth: 46142
```

```
python driver.py dfs 8,6,4,2,1,3,5,7,0

path_to_goal: ['Up', 'Up', 'Left', ..., , 'Up', 'Up', 'Left']
cost_of_path: 9612
nodes_expanded: 9869
search_depth: 9612
max_search_depth: 9612
```

### Example 3: A-Star Search

Note that in general, for any initial board whatsoever, for BFS and DFS there is one and only one correct answer. For A*, however, the output of nodes_expanded may vary a little, depending on specific implementation details.

The following two test cases are used for program validation. Note that long path_to_goals are truncated and running_time/max_ram_usage are removed:


```
python driver.py ast 6,1,8,4,0,2,7,3,5

path_to_goal: ['Down', 'Right', 'Up', 'Up', 'Left', 'Down', 'Right', 'Down', 'Left', 'Up', 'Left', 'Up', 'Right', 'Right', 'Down', 'Down', 'Left', 'Left', 'Up', 'Up']
cost_of_path: 20
nodes_expanded: 696
search_depth: 20
max_search_depth: 20
```

```
python driver.py ast 8,6,4,2,1,3,5,7,0

path_to_goal: ['Left', 'Up', 'Up', 'Left', 'Down', 'Right', 'Down', 'Left', 'Up', 'Right', 'Right', 'Up', 'Left', 'Left', 'Down', 'Right', 'Right', 'Up', 'Left', 'Down', 'Down', 'Right', 'Up', 'Left', 'Up', 'Left']
cost_of_path: 26
nodes_expanded: 1585
search_depth: 26
max_search_depth: 26
```
