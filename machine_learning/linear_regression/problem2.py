import numpy as np
import sys

def process_input_rows(input_rows):
    rows = np.array(input_rows)
    means = np.mean(rows, axis = 0)
    stds = np.std(rows, axis = 0)
    for row in input_rows:
        row[0] = (row[0] - means[0]) / stds[0]
        row[1] = (row[1] - means[1]) / stds[1]

def linear_regression(rows, alpha, number_of_iterations):
    b_0,b_age,b_weight = 0,0,0
    n = len(rows)

    for i in range(number_of_iterations):
        db_0,db_age,db_weight = 0, 0, 0
        for j in range(n):
            diff = b_0+b_age*rows[j][0]+b_weight*rows[j][1]-rows[j][2]
            db_0 += diff
            db_age+= diff*rows[j][0]
            db_weight+= diff*rows[j][1]
        b_0 -= alpha * db_0 / n
        b_age -= alpha * db_age / n
        b_weight -= alpha * db_weight / n
    return [alpha, number_of_iterations, b_0, b_age, b_weight]

def run_linear_regression_experiments(input_rows):
    learning_rates = [0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1, 5, 10, 100]
    output_rows = []
    for rate in learning_rates:
        output_rows.append(linear_regression(input_rows, rate, 100))
    return output_rows

if __name__ == "__main__":
  if len(sys.argv) == 3:
    with open(sys.argv[1], "r") as fin:
        lines = fin.readlines()
        input_rows = []
        for line in lines:
            input_rows.append(map(float, line.split(",")))
        fin.close()

        process_input_rows(input_rows)

        output_rows = run_linear_regression_experiments(input_rows)
        with open(sys.argv[2], "w") as fout:
            for row in output_rows:
                fout.write("%s\n" % (",".join([str(i) for i in row])))
            fout.close()
