from sklearn import metrics
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier

import numpy as np
import sys

defaultTestSize = 0.4
defaultNSplits = 5

def grid_search(method_name, estimator, param_grid, X, y):
    X, y = np.array(X), np.array(y)
    sss = StratifiedShuffleSplit(n_splits=defaultNSplits, test_size=defaultTestSize, random_state=0)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=defaultTestSize, random_state=0)
    clf = GridSearchCV(estimator, param_grid, cv=sss)
    clf.fit(X_train, y_train)
    y_train_true, y_train_pred = y_train, clf.predict(X_train)
    y_test_true, y_test_pred = y_test, clf.predict(X_test)
    train_score = metrics.accuracy_score(y_train_true, y_train_pred)
    test_score = metrics.accuracy_score(y_test_true, y_test_pred)
    return [method_name, train_score, test_score]

def svm_linear(X, y):
    param_grid = [
        {'kernel': ['linear'], 'C': [0.1, 0.5, 1, 5, 10, 50, 100]},
    ]
    return grid_search("svm_linear", SVC(), param_grid, X, y)

def svm_polynomial(X, y):
    param_grid = [
        {'kernel': ['poly'], 'C': [0.1, 1, 3], 'degree' : [4, 5, 6], 'gamma': [0.1, 0.5]},
    ]
    return grid_search("svm_polynomial", SVC(), param_grid, X, y)

def svm_rbf(X, y):
    param_grid = [
        {'kernel': ['rbf'], 'C': [0.1, 0.5, 1, 5, 10, 50, 100], 'gamma': [0.1, 0.5, 1, 3, 6, 10]},
    ]
    return grid_search("svm_rbf", SVC(), param_grid, X, y)

def logistic(X, y):
    param_grid = [
        {'C': [0.1, 0.5, 1, 5, 10, 50, 100]},
    ]
    return grid_search("logistic", LogisticRegression(), param_grid, X, y)

def knn(X, y):
    param_grid = [
        {'n_neighbors': range(1, 51), 'leaf_size': np.arange(5, 61, 5)},
    ]
    return grid_search("knn", KNeighborsClassifier(), param_grid, X, y)

def decision_tree(X, y):
    param_grid = [
        {'max_depth': range(1, 51), 'min_samples_split': range(2, 11)},
    ]
    return grid_search("decision_tree", DecisionTreeClassifier(), param_grid, X, y)

def random_forest(X, y):
    param_grid = [
        {'max_depth': range(1, 51), 'min_samples_split': range(2, 11)},
    ]
    return grid_search("random_forest", RandomForestClassifier(), param_grid, X, y)

def perform_algorithms(X, y):
    output_rows = [
        svm_linear(X, y),
        svm_polynomial(X, y),
        svm_rbf(X, y),
        logistic(X, y),
        knn(X, y),
        decision_tree(X, y),
        random_forest(X, y)
    ]
    return output_rows

if __name__ == "__main__":
    with open("input3.csv", "r") as fin:
        lines = fin.read().split("\r")
        X,y = [],[]
        for line in lines:
            if len(line.split(",")) == 3:
                data = map(float, line.split(","))
                X.append(data[:2])
                y.append(data[2])
        fin.close()

        output_rows = perform_algorithms(X, y)
        print output_rows

        with open("output3.csv", "w") as fout:
            for row in output_rows:
                fout.write("%s\n" % (",".join([str(i) for i in row])))
            fout.close()
