import sys

def perceptron_learning_algorithm(input_rows):
    b,w0,w1 = 0,0,0
    output_rows = []
    modified = True
    while modified:
        modified = False
        for row in input_rows:
            f = 1 if w0 * row[0] + w1 * row[1] + b > 0 else -1
            if f * row[2] <= 0:
                modified = True
                if row[2] > 0:
                    w0 += row[0]
                    w1 += row[1]
                    b += row[2]
                else:
                    w0 -= row[0]
                    w1 -= row[1]
                    b -= row[2]
                output_rows.append([w0, w1, b])
    return output_rows

if __name__ == "__main__":
  if len(sys.argv) == 3:
    with open(sys.argv[1], "r") as fin:
        lines = fin.readlines()
        input_rows = []
        for line in lines:
            input_rows.append(map(int, line.split(",")))
        fin.close()

        output_rows = perceptron_learning_algorithm(input_rows)
        with open(sys.argv[2], "w") as fout:
            for row in output_rows:
                fout.write("%s\n" % (",".join([str(i) for i in row])))
            fout.close()
