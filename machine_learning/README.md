## Perceptron Learning Algorithm

`problem1.py` implements the perceptron learning algorithm ("PLA") for a linearly separable dataset. `input1.csv` contains a series of data points. Each point is a comma-separated ordered triple, representing feature_1, feature_2, and the label for the point. The values of the features are the x- and y-coordinates of each point. The label takes on a value of positive or negative one. The label are separating the points into two categories.

The program could be executed like so:

```
$ python problem1.py input1.csv output1.csv
```

This should generate an output file called `output1.csv`. With each iteration of the PLA (each time we go through all examples), the program will print a new line to the output file, containing a comma-separated list of the weights w_1, w_2, and b in that order. Upon convergence, the program will stop, and the final values of w_1, w_2, and b will be printed to the output file. This defines the decision boundary that the PLA has computed for the given dataset.

## Linear Regression

`problem2.py` implements the linear regression with multiple features using gradient descent. `input2.csv` contains a series of data points. Each point is a comma-separated ordered triple, representing age, weight, and height (derived from CDC growth charts data).

### Data Preparation and Normalization.

Scale each feature (i.e. age and weight) by its (population) standard deviation, and set its mean to zero. For each feature x (a column in your data matrix), use the following formula for scaling:

```
x_scaled = (x - mean(x)) / stdev(x)
```

### Gradient Descent.

Run the gradient descent algorithm using the following learning rates: α ∈ {0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1, 5, 10}. For each value of α, run the algorithm for exactly 100 iterations.

The program could be executed like so:

```
$ python problem2.py input2.csv output2.csv
```

This should generate an output file called `output2.csv`. There are ten cases in total. After each of these ten runs, the program prints a new line to the output file, containing a comma-separated list of alpha, number_of_iterations, b_0, b_age, and b_weight in that order. These represent the regression models that the gradient descents have computed for the given dataset.

## Classification

`problem3.py` uses the support vector classifiers in the sklearn package to learn a classification model for a chessboard-like dataset. `input3.csv` contains a series of data points. In the program, it uses SVM with different kernels to build a classifier. The data is split into training (60%) and testing (40%) using stratified sampling. Instead of a validation set, we use cross validation with the number of folds k = 5.

### SVM with Linear Kernel.

Observe the performance of the SVM with linear kernel. Search for a good setting of parameters to obtain high classification accuracy. Specifically, try values of C = [0.1, 0.5, 1, 5, 10, 50, 100]. This is accomplished by using sklearn.grid_search. After locating the optimal parameter value by using the training data, record the corresponding best score (training data accuracy) achieved. Then apply the testing data to the model, and record the actual test score.

### SVM with Polynomial Kernel.

Try values of C = [0.1, 1, 3], degree = [4, 5, 6], and gamma = [0.1, 0.5].

### SVM with RBF Kernel.

Try values of C = [0.1, 0.5, 1, 5, 10, 50, 100] and gamma = [0.1, 0.5, 1, 3, 6, 10].

### Logistic Regression.

Try values of C = [0.1, 0.5, 1, 5, 10, 50, 100].

### k-Nearest Neighbors.

Try values of n_neighbors = [1, 2, 3, ..., 50] and leaf_size = [5, 10, 15, ..., 60].

### Decision Trees.

Try values of max_depth = [1, 2, 3, ..., 50] and min_samples_split = [2, 3, 4, ..., 10].

### Random Forest.

Try values of max_depth = [1, 2, 3, ..., 50] and min_samples_split = [2, 3, 4, ..., 10].
