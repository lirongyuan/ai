from random import randint
from BaseAI import BaseAI

import math
import time

class PlayerAI(BaseAI):
    def getMove(self, grid):
        return self.iterativeDeepeningSearch(grid, 4, 5)

    def iterativeDeepeningSearch(self, grid, minDepth, maxDepth):
        bestMove = None
        bestValue = float("-inf")
        for depth in range(minDepth, maxDepth):
            move,value = self.maxValue(grid, float("-inf"), float("inf"), depth)
            if value >= bestValue:
              bestValue = value
              bestMove = move
        return bestMove

    def maxValue(self, grid, alpha, beta, depth):
        bestValue = float("-inf")
        bestMove = None
        for move in range(grid.size):
            gridCopy = grid.clone()
            if gridCopy.move(move):
                tempValue = self.minValue(gridCopy, alpha, beta, depth - 1)
                alpha = max(alpha, tempValue)
                if tempValue >= beta:
                    return move, tempValue
                if tempValue >= bestValue:
                    bestValue = tempValue
                    bestMove = move
        return bestMove, bestValue

    def minValue(self, grid, alpha, beta, depth):
        if depth <= 0:
            return self.heuristic(grid)
        cells = self.worstCells(grid)
        value = float("inf")
        for cell in cells:
            for tile in [2,4]:
                gridCopy = grid.clone()
                gridCopy.setCellValue(cell,tile)
                move, tempValue = self.maxValue(gridCopy, alpha, beta, depth)
                value = min(value, tempValue)
                if value <= alpha:
                    return value
                beta = min(beta, value)
        return value

    def worstCells(self, grid):
        maxVal = -1
        cells = []
        for x in xrange(grid.size):
            for y in xrange(grid.size):
                if grid.map[x][y] == 0:
                    val = 0
                    for xx in range(-1,2):
                        for yy in range(-1,2):
                            if not grid.crossBound((x+xx,y+yy)):
                                val += grid.map[x+xx][y+yy]
                    if val > maxVal:
                        cells = [(x,y)]
                        maxVal = val
                    elif val == maxVal:
                        cells.append((x,y))
        return cells if len(cells) > 0 else None

    def heuristic(self, grid):
        numOfEmptyCells = 0
        diffOfAdjacentCells = 1
        maxVal = 1
        for x in xrange(grid.size):
            for y in xrange(grid.size):
                currVal = grid.map[x][y]
                maxVal = max(maxVal, currVal)
                if currVal == 0:
                    numOfEmptyCells += 1
                else:
                    for xx in range(0,2):
                        for yy in range(0,2):
                            if not grid.crossBound((x+xx,y+yy)):
                                neighborVal = grid.map[x+xx][y+yy]
                                diff = math.fabs(neighborVal - currVal)
                                diffOfAdjacentCells += diff

        smoothness = - math.log(diffOfAdjacentCells, 4)
        openness = math.log(numOfEmptyCells)
        monotonicity = self.monotonicity(grid)
        return monotonicity + 2.7 * openness + 0.1 * smoothness + maxVal

    def monotonicity(self, grid):
        scores = [0, 0, 0, 0]
        # up/down direction
        for x in xrange(grid.size):
            currentIdx = 0
            nextIdx = currentIdx + 1
            while nextIdx < grid.size:
                while nextIdx < grid.size and grid.map[x][nextIdx] == 0:
                    nextIdx += 1
                if nextIdx >= grid.size:
                    nextIdx -= 1
                currentVal = math.log(grid.map[x][currentIdx],2) if grid.map[x][currentIdx] != 0 else 0
                nextVal = math.log(grid.map[x][nextIdx],2) if grid.map[x][nextIdx] != 0 else 0
                if currentVal > nextVal:
                    scores[0] += nextVal - currentVal
                elif nextVal > currentVal:
                    scores[1] += currentVal - nextVal
                currentIdx = nextIdx
                nextIdx += 1
        # left/right direction
        for y in xrange(grid.size):
            currentIdx = 0
            nextIdx = currentIdx + 1
            while nextIdx < grid.size:
                while nextIdx < grid.size and grid.map[nextIdx][y] == 0:
                    nextIdx += 1
                if nextIdx >= grid.size:
                    nextIdx -= 1
                currentVal = math.log(grid.map[currentIdx][y],2) if grid.map[currentIdx][y] != 0 else 0
                nextVal = math.log(grid.map[nextIdx][y],2) if grid.map[nextIdx][y] != 0 else 0
                if currentVal > nextVal:
                    scores[2] += nextVal - currentVal
                elif nextVal > currentVal:
                    scores[3] += currentVal - nextVal
                currentIdx = nextIdx
                nextIdx += 1

        return max(scores[0], scores[1]) + max(scores[2], scores[3])
