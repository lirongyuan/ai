## 2048-puzzle

An instance of the 2048-puzzle game is played on a 4×4 grid, with numbered tiles that slide in all four directions when a player moves them. Every turn, a new tile will randomly appear in an empty spot on the board, with a value of either 2 or 4. Per the input direction given by the player, all tiles on the grid slide as far as possible in that direction, until they either (1) collide with another tile, or (2) collide with the edge of the grid. If two tiles of the same number collide while moving, they will merge into a single tile, valued at the sum of the two original tiles that collided. The resulting tile cannot merge with another tile again in the same move. If you have not played the game before, you may do so at [gabrielecirulli.github.io/2048](https://gabrielecirulli.github.io/2048) to get a sense of how the game works.

With typical board games like chess, the two players in the game (i.e. the "Computer AI" and the "Player") take similar actions in their turn, and have similar objectives to achieve in the game. In the 2048-puzzle game, the setup is inherently asymmetric; that is, the computer and player take drastically different actions in their turns. Specifically, the computer is responsible for placing random tiles of 2 or 4 on the board, while the player is responsible for moving the pieces. However, adversarial search can be applied to this game just the same.

In the 2048-puzzle game, the computer AI is technically not "adversarial". In particular, all it does is spawn random tiles of 2 and 4 each turn, with a designated probability of either a 2 or a 4; it certainly does not specifically spawn tiles at the most inopportune locations to foil the player's progress. However, for simplicity, the "Player AI" will play as if the computer is completely adversarial. In particular, the minimax algorithm is employed.

- GameManager.py. This is the driver program that loads Computer AI and Player AI, and begins a game where they compete with each other.
- Grid.py. This module defines the Grid object, along with some useful operations: move(), getAvailableCells(), insertTile(), and clone().
- BaseAI.py. This is the base class for any AI component. All AIs inherit from this module, and implement the getMove() function, which takes a Grid object as parameter and returns a move (there are different "moves" for different AIs).
- ComputerAI.py. This inherits from BaseAI. The getMove() function returns a computer action that is a tuple (x, y) indicating the place to place a tile.
- PlayerAI.py. This inherits from BaseAI. The getMove() function returns a number that indicates the player’s action. In particular, 0 stands for "Up", 1 stands for "Down", 2 stands for "Left", and 3 stands for "Right".
- BaseDisplayer.py and Displayer.py. These print the grid.

To run the code, execute the game manager like so:

```
$ python GameManager.py
```

The progress of the game will be displayed on the terminal screen, with one snapshot printed after each move that the Computer AI or Player AI makes. The Player AI is allowed 0.2 seconds to come up with each move. The process continues until the game is over; that is, until no further legal moves can be made. At the end of the game, the maximum tile value on the board is printed.

The implementation of the Player AI has the following properties:

- Employed the minimax algorithm.
- Implemented alpha-beta pruning, which speeds up the search process by eliminating irrelevant branches.
- Used heuristic functions. In this game it is highly impracticable to search the entire depth of the theoretical game tree. To be able to cut off the search at any point, heuristic functions are employed to assign approximate values to nodes in the tree.
- Assigned heuristic weights. There are more than one heuristic function, thus need to assign weights associated with each individual heuristic with careful reasoning and careful experimentation.
