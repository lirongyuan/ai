# README

## Clone this repository

```
# Over HTTPS:
git clone https://bitbucket.org/lirongyuan/ai.git

# Over SSH:
git clone git@bitbucket.org:lirongyuan/ai.git

```

## Machine Learning

- Perceptron Learning Algorithm: [problem1.py](machine_learning/perceptron_learning_algorithm/problem1.py) implements the perceptron learning algorithm ("PLA") for a linearly separable dataset.
- Linear Regression: [problem2.py](machine_learning/linear_regression/problem2.py) implements the linear regression with multiple features using gradient descent.
- Classification: [problem3.py](machine_learning/classification/problem3.py) uses the support vector classifiers in the sklearn package to learn a classification model for a chessboard-like dataset.

## 2048-puzzle

[PlayerAI.py](2048_puzzle/PlayerAI.py) could intelligently play the 2048-puzzle game using advanced techniques of adversarial search to probe the search space.

## 8-puzzle game

[driver.py](8_puzzle_game/driver.py) could solve any 8-puzzle board with breadth-first search, depth-first search, or A-star search when given an arbitrary starting configuration.
